﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QuanLyDiemHs.Data;
using QuanLyDiemHs.Models;

namespace QuanLyDiemHs.Controllers
{
    public class HocSinhsController : Controller
    {
        private readonly QuanLyDiemContext _context;

        public HocSinhsController(QuanLyDiemContext context)
        {
            _context = context;
        }

        // GET: HocSinhs
        public async Task<IActionResult> Index()
        {
              return View(await _context.HocSinh.ToListAsync());
        }

        // GET: HocSinhs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.HocSinh == null)
            {
                return NotFound();
            }

            var hocSinh = await _context.HocSinh
                .FirstOrDefaultAsync(m => m.MaHs == id);
            if (hocSinh == null)
            {
                return NotFound();
            }

            return View(hocSinh);
        }

        // GET: HocSinhs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: HocSinhs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MaHs,HoTen,GioiTinh,NgaySinh,SDT,DiaChi")] HocSinh hocSinh)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hocSinh);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(hocSinh);
        }

        // GET: HocSinhs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.HocSinh == null)
            {
                return NotFound();
            }

            var hocSinh = await _context.HocSinh.FindAsync(id);
            if (hocSinh == null)
            {
                return NotFound();
            }
            return View(hocSinh);
        }

        // POST: HocSinhs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MaHs,HoTen,GioiTinh,NgaySinh,SDT,DiaChi")] HocSinh hocSinh)
        {
            if (id != hocSinh.MaHs)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hocSinh);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HocSinhExists(hocSinh.MaHs))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(hocSinh);
        }

        // GET: HocSinhs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.HocSinh == null)
            {
                return NotFound();
            }

            var hocSinh = await _context.HocSinh
                .FirstOrDefaultAsync(m => m.MaHs == id);
            if (hocSinh == null)
            {
                return NotFound();
            }

            return View(hocSinh);
        }

        // POST: HocSinhs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.HocSinh == null)
            {
                return Problem("Entity set 'QuanLyDiemContext.HocSinh'  is null.");
            }
            var hocSinh = await _context.HocSinh.FindAsync(id);
            if (hocSinh != null)
            {
                _context.HocSinh.Remove(hocSinh);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HocSinhExists(int id)
        {
          return _context.HocSinh.Any(e => e.MaHs == id);
        }
    }
}
