﻿using Microsoft.AspNetCore.Mvc;

namespace QuanLyDiemHs.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
