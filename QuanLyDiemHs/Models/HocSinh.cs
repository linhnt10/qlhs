﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace QuanLyDiemHs.Models
{
    public class HocSinh
    {
        [Key]
        public int MaHs { get; set; }

        [Display(Name = "Họ tên")]
        public string HoTen { get; set; }
        public bool GioiTinh { get; set; }

        [Display(Name = "Ngày sinh")]
        public DateTime NgaySinh { get; set; }

        [Display(Name = "Số điện thoại")]
        public int SDT { get; set; }

        [Display(Name = "Địa chỉ")]
        public string DiaChi { get; set; }



    }
}
