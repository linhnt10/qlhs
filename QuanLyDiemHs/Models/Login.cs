﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace QuanLyDiemHs.Models
{
    public class Login
    {
        [Key, Column(Order = 1)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int IdUser { get; set; }

        [Display(Name = "User")]
        public string User { get; set; }

        [Display(Name = "Password")]
        public string Password { get; set; }

    }
}
