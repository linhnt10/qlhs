﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace QuanLyDiemHs.Models
{
    public class KetQua
    {
        [Key]
        public int MaHs { get; set; }
        public string TenHs { get; set; }
        public int MaMh { get; set; }
        public string TenMh { get; set; }
        public double DiemThi { get; set; }
    }
}
