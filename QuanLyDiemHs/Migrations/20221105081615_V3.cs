﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace QuanLyDiemHs.Migrations
{
    public partial class V3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiemSo",
                table: "MonHoc");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DiemSo",
                table: "MonHoc",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
